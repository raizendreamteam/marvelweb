import { Component, OnInit } from '@angular/core';
import { LoginService } from './services/login.service';
import { User } from './models/User';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  status: boolean;
  user: User = new User();

  constructor(
    private loginService: LoginService,
    public router: Router,
    public localStorageService: LocalStorageService) { }

  ngOnInit(): void {
    if (this.localStorageService.getOnLocalStorage('userLogged') === true) {
     alert('Você está logado');
     this.router.navigate(['filter']);
    }
  }

  public OnPress() {
    this.status = this.loginService.Auth(this.user.UserName, this.user.Password);
    if (this.status === true) {
      this.localStorageService.setOnLocalStorage('userLogged', true);
      this.router.navigate(['filter']);
    } else {
      this.localStorageService.setOnLocalStorage('userLogged', false);
      alert('Usuario ou senha errado(s)');
    }
  }
}
