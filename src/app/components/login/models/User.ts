export class User {
  private username: string;
  private password: string;

  public get UserName(): string {
    return this.username;
  }
  public set UserName(value: string) {
    this.username = value;
  }

  public get Password(): string {
    return this.password;
  }
  public set Password(value: string) {
    this.password = value;
  }
}
